
var fs = require('fs');

var path = 'E:\\Projetos em Node.js\\Udemy-Unity-RESTAPI_NodeJS\\PromiseAula';
var file = 'meuArquivo.txt';

var pathFile = path +  '\\' + file;

console.log('processo 1');

//#region
// fs.writeFile(pathFile, 'Hello!', function(error){

//     if (error){
//         throw error;
//     }

//     console.log('processo 2');
// });
//#endregion

myWriteFile(pathFile).then((result) => {

    console.log(result);
    console.log('processo 3');
    
}).catch((err) => {

    console.log(err);
    console.log('processo 4');

});

// console.log('processo 3');


//------------FUNCTIONS----------------//

function myWriteFile(p_File){
    return new Promise((resolve, reject) => {

        if (p_File == ""){
            reject("Path and file are empty!");
        }

        fs.writeFile(p_File, 'HellodARKNS', function(error){

            if (error){
                // throw error;
                reject(error);
            }
        
            resolve('Process - 2');
        });

    });
}